<?php
include 'func.php';
include 'PDO.connect.php';
//ini_set("display_errors", 0);

$act = trim($_GET['act']);
switch ($act) {
    case 'login':
        $input = array(
            'password' => $_POST['password'],
            'id' => $_POST['id']
        );
        $status = login($pdo, $input);
        echo json_encode($status);
        break;
    case 'check-auth':
        $input = array(
            'token' => $_POST['token'],
            'id' => $_POST['id']
        );
        $status = checkAuth($pdo, $input);
        echo json_encode($status);
        break;
    case 'get-posts':
        $count = (isset($_GET['c'])) ? (int)$_GET['c'] : NULL;
        $type = (isset($_GET['t'])) ? $_GET['t'] : 'project';
    
        $status = getPosts($pdo, $count, $type);
        echo json_encode($status);
        break;
    case 'get-post':
        $input = array(
            'type' => $_POST['type'],
            'id' => $_POST['id']
        );
        $status = getPost($pdo, $input);
        echo json_encode($status);
        break;
    case 'get-post-advance':
        $input = array(
            'type' => $_POST['type'],
            'url' => $_POST['url']
        );
        $status = getPostAdvance($pdo, $input);
        echo json_encode($status);
        break;
    case 'upload-image':
        $status = array('file' => $_FILES);
        echo json_encode($status);
        break;
    case 'create-post':
        $input = array(
            'type' =>          $_POST['type'],
            'title' =>         $_POST['title'],
            'preview_text' =>  $_POST['preview_text'],
            'post_type' =>     $_POST['post_type'],
            'preview_image' => $_POST['preview_image'],
            'url' =>           $_POST['url'],
            'date' =>          $_POST['date']
        );
    
        $allowableTags = '<pre><ul><li><b><i><p><a><blockquote><h1><h2><h3><img><span><center>';
        $html = htmlspecialchars(strip_tags(trim($_POST['full_text']), $allowableTags));
        
        $status = createPost($pdo, $input, $html);
        echo json_encode($status);
        break;
    case 'update-post':
        $input = array(
            'type' =>          $_POST['type'],
            'title' =>         $_POST['title'],
            'preview_text' =>  $_POST['preview_text'],
            'post_type' =>     $_POST['post_type'],
            'preview_image' => $_POST['preview_image'],
            'url' =>           $_POST['url'],
            'date' =>          $_POST['date'],
            'id' =>            $_POST['id']
        );
    
        $allowableTags = '<pre><ul><li><b><i><p><a><blockquote><h1><h2><h3><img><span><center>';
        $html = htmlspecialchars(strip_tags(trim($_POST['full_text']), $allowableTags));
        
        $status = updatePost($pdo, $input, $html);
        echo json_encode($status);
        break;
    case 'remove-post':
        $input = array(
            'type' => $_POST['type'],
            'id' => (int)$_POST['id']
        );
    
        $status = removePost($pdo, $input);
        echo json_encode($status);
        break;
    default:
        echo json_encode(array('status' => 0, 'error' => 'Unknown event'));
        break;
}