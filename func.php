<?php
header('Content-Type: text/html; charset=utf-8');

function cookie($name, $source){
    setcookie($name, $source, time()+3600 * 24 * 1000);
}

function prepareInput ($input) {
    foreach ($input as &$value) {
        $value = trim(strip_tags($value));
    }
    return $input;
}

function login ($pdo, $input) {
    $input = prepareInput($input);
    $query = $pdo->prepare('SELECT * FROM admin WHERE id = :id AND password = :password');
    $query->execute(array('password' => md5($input['password']), 'id' => $input['id']));
    
    if ($query->rowCount()) {
        cookie('token', md5($input['password']));
        cookie('id', $input['id']);
        return array('status' => 1);
    }
    return array('status' => 0);
}

function checkAuth ($pdo, $input) {
    $input = prepareInput($input);
    $query = $pdo->prepare('SELECT * FROM admin WHERE id = :id AND password = :password');
    $query->execute(array('password' => $input['token'], 'id' => $input['id']));
    
    if ($query->rowCount()) {
        return array('status' => 1);
    }
    return array('status' => 0);
}

function getPosts ($pdo, $count, $type) {
    $limit = ($count) ? ' LIMIT :limit' : '';
    
    $query = $pdo->prepare('SELECT * FROM '.$type.' WHERE 1 ORDER BY date'.$limit);
    if ($count)
        $query->bindParam('limit', $count, PDO::PARAM_INT);
    
    $query->execute();
    
    // to int
    $array = $query->fetchAll();
    for ($i = 0; $i < count($array); $i++) {
        $array[$i]['id'] = (int)$array[$i]['id'];
        $array[$i]['date'] = (double)$array[$i]['date'];
    }
    
    if ($query->rowCount()) {
        $status = array('status' => 1, $type => $array);
    }
    else
        $status = array('status' => 0);
    
    return $status;
}

function getPost ($pdo, $input) {
    $input = prepareInput($input);
    
    $query = $pdo->prepare('SELECT * FROM '.$input['type'].' WHERE id = :id');
    $query->execute(array('id' => $input['id']));
    
    if ($query->rowCount()) {
        $array = $query->fetch();
        $array['full_text'] = htmlspecialchars_decode($array['full_text']);
        $status = array('status' => 1, 'post' => $array);
    }
    else
        $status = array('status' => 0);
    
    return $status;
}

function getPostAdvance($pdo, $input) {
    $input = prepareInput($input);
    
    $query = $pdo->prepare('SELECT(@id := id) as id, (SELECT url FROM '.$input['type'].' WHERE id = @id + 1) as next_url,(SELECT url FROM '.$input['type'].' WHERE id = @id - 1) as prev_url, (SELECT title FROM '.$input['type'].' WHERE id = @id + 1) as next_title,(SELECT title FROM '.$input['type'].' WHERE id = @id - 1) as prev_title,`title`,`type`,`preview_image`,`preview_text`,`url`,`full_text`,`date`FROM '.$input['type'].' WHERE url = :url');
    $query->execute(array('url' => $input['url']));
    
    if ($query->rowCount()) {
        $array = $query->fetch();
        $array['full_text'] = htmlspecialchars_decode($array['full_text']);
        $array['next'] = array('title' => $array['next_title'], 'url' => $array['next_url']);
        $array['prev'] = array('title' => $array['prev_title'], 'url' => $array['prev_url']);
        unset($array['next_url'], $array['prev_url'], $array['next_title'], $array['prev_title']);
        
        $status = array('status' => 1, 'post' => $array);
    } else
        $status = array('status' => 0);
    
    return $status;
}

function createPost($pdo, $input, $html) {
    $input = prepareInput($input);
    
    $user = array(
        'id' => $_COOKIE['id'],
        'token' => $_COOKIE['token']
    );
    if (checkAuth($pdo, $user)['status']) {
        $query = $pdo->prepare('INSERT INTO `'.$input['post_type'].'` (`title`, `type`, `preview_image`, `preview_text`, `url`, `full_text`, `date`) VALUES (:title, :type, :previewImg, :previewText, :url, :fullText, :date)');
        $query->execute(array('title' => $input['title'], 'type' => $input['type'], 'previewImg' => $input['preview_image'], 'previewText' => $input['preview_text'], 'url' => $input['url'], 'fullText' => $html, 'date' => $input['date']));
        
        $status = ($pdo->lastInsertId()) ? array('status' => 1) : array('status' => 0);
    } else $status = array('auth' => 0);
    return $status;
}

function updatePost($pdo, $input, $html) {
    $input = prepareInput($input);
    
    $user = array(
        'id' => $_COOKIE['id'],
        'token' => $_COOKIE['token']
    );
    if (checkAuth($pdo, $user)['status']) {
        $query = $pdo->prepare('UPDATE `'.$input['post_type'].'` SET `title`= :title,`type`= :type, `preview_image`= :previewImg, `preview_text`= :previewText, `url`= :url, `full_text`= :fullText, `date`= :date  WHERE id = :id');
        $flag = $query->execute(array('title' => $input['title'], 'type' => $input['type'], 'previewImg' => $input['preview_image'], 'previewText' => $input['preview_text'], 'url' => $input['url'], 'fullText' => $html, 'date' => $input['date'], 'id' => $input['id']));
        
        $status = ($flag) ? array('status' => 1) : array('status' => 0);
    } else $status = array('auth' => 0);
    return $status;
}

function removePost ($pdo, $input) {
    $input = prepareInput($input);
    
    $user = array(
        'id' => $_COOKIE['id'],
        'token' => $_COOKIE['token']
    );
    if (checkAuth($pdo, $user)['status']) {
        $query = $pdo->prepare('DELETE FROM `'.$input['type'].'` WHERE id = :id');
        $flag = $query->execute(array('id' => $input['id']));
        
        $status = ($flag) ? array('status' => 1) : array('status' => 0);
    }
    
    return $status;
}