/* 
    Модуль многоязычности сайта.
    Vlad Simonov, 2016
*/
angular.module('translation', []).service('translationService', function ($resource) {
    this.getTranslation = function($rootScope, language) {
            var languageFilePath = 'lang/' + language + '.json';
            $resource(languageFilePath).get(function (data) {
                $rootScope.lang = data;
                $rootScope.domain = 'http://localhost/smnv.space';
                currentLang = data;
            });
        };
});