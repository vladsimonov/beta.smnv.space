/*
    Контроллер главной страницы сайта.
    Vlad Simonov, 2016
*/
var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Получение последних трех записей из проектов
    function getPosts () {
        // По умолчанию возвращаются все записи, c=3 указывается для получения последних трёх
        $.post('api?act=get-posts&c=3', function (response) {
            var object = JSON.parse(response);
            if (object.status) {
                $scope.projects = object.project;
                $scope.$apply();
            }
        });
    }
    getPosts();
}]);