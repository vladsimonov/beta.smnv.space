/*
    Контроллер страницы отдельной записи.
    Vlad Simonov, 2016
*/
var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Получение записи по URL
    function getPost () {
        $.post('api?act=get-post-advance', $scope.params, function (response) {
            var object = JSON.parse(response);
            console.log(object);
            if (object.status) {
                $scope.post = object.post;
                $('.post-full').html(object.post.full_text);
                $scope.$apply();
            } else document.location.href = $scope.domain;
        });
    }
    setTimeout(function () {
        getPost();
    }, 200);
}]);