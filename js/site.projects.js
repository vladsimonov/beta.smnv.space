/*
    Контроллер страницы проектов.
    Vlad Simonov, 2016
*/
var currentLang;

var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Получает все записи из проектов для отображения превью
    function getPosts () {
        // По умолчанию POST запрос по URL 'api?act=get-posts' вернет записи проектов,
        // дополнительных параметров t= не требуется, или t=projects
        $.post('api?act=get-posts', function (response) {
            var object = JSON.parse(response);
            if (object.status) {
                $scope.projects = object.project;
                $scope.$apply();
            }
        });
    }
    getPosts();
}]);