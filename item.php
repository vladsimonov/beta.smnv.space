<html ng-app="app" ng-controller="appCtrl">
<head>
    <link rel="icon" type="image/png" href="favicon.png" />
    <title ng-bind="post.title">Post</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/media-queries.css" type="text/css">
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-cookies.min.js"></script>
    <script src="js/angular-resource.min.js"></script>
    <script src="js/angular-translate.js"></script>
    <script src="js/common.js"></script>
    <script src="js/site.item.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes">
</head>
<body style="overflow: hidden" ng-init="params = {type: '<?=$_GET['type']?>', url: '<?=$_GET['url']?>'}">
    <div class="preload">
        <div class="loading"></div>
    </div>
    <ng-include src="'tpl/header.html'"></ng-include>
    <section class="post" ng-class="(params.type == 'project') ? 'project' : 'blog'">
        <div class="post-head">
            <div class="navigation-box">
                <a ng-href="{{post.prev.url}}" ng-show="post.prev.url">
                    <div class="navigation-item left-navig" ng-class="(post.prev.title.length <= 22) ? 'one-liner' : false">
                        <span>{{post.prev.title}}</span>
                        <span class="adaptive-navig">{{lang.PREV}}</span>
                    </div>
                </a>
            </div>
            <div class="post-title">
                <div class="post-type">{{post.type}}</div>
                <div class="post-name">{{post.title}}</div>
            </div>
            <div class="navigation-box">
                <a ng-href="{{post.next.url}}" ng-show="post.next.url">
                    <div class="navigation-item right-navig" ng-class="(post.next.title.length <= 22) ? 'one-liner' : false">
                        <span>{{post.next.title}}</span>
                        <span class="adaptive-navig">{{lang.NEXT}}</span>
                    </div>
                </a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="post-full"></div>
        <div class="share">
            <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
        </div>
    </section>
    <section class="projects">
        <div class="project-item" ng-repeat="w in blogs">
            <div class="container">
                <div class="project-image" ng-style="{'background-image': 'url(' + w.preview_image + ')'}"></div>
                <div class="project-info">
                    <div class="title">{{w.title}}</div>
                    <div class="category">{{w.type}}</div>
                    <div class="about">
                        {{w.preview_text}}
                    </div>
                    <a ng-href="{{w.url}}"><div class="button">{{lang.VIEW_PROJECT}}</div></a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    <ng-include src="'tpl/footer.html'"></ng-include>
    <script type="text/javascript">(function() {
      if (window.pluso)if (typeof window.pluso.start == "function") return;
      if (window.ifpluso==undefined) { window.ifpluso = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
      }})();
    </script>
</body>
</html>