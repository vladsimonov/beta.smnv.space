<?php
ini_set("display_errors", 0);
error_reporting( E_ERROR );

// uploads path
$UPLOAD_PATH = 'images/upload/';

// получаем расширение файла
$ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

if ( 0 < $_FILES['file']['error'] ) {
    $status = array('status' => 0, 'error' => 'File error', 'code' => $_FILES['file']['error']);
}
else {
    if ($_FILES['upload_file']['size'] < 5242880 && $_FILES['file'] != NULL) {
        $name = md5(time().'salt'.mktime());
        switch ($ext) {
            case 'jpeg':
            case 'jpg':
                $image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
                imagejpeg($image, $UPLOAD_PATH.$name.'.'.$ext, 75);
                imagedestroy($image);
                break;
            case 'png':
            case 'gif':
                move_uploaded_file($_FILES['file']['tmp_name'], $UPLOAD_PATH.$name.'.'.$ext);
                break;
        }
        $status = array('status' => 1, 'src' => $UPLOAD_PATH.$name.'.'.$ext);
    }
    else $status = array('status' => 0, 'error' => 'Image size is too large or empty.');
}

echo json_encode($status);

?>