/*
    Контроллер страницы входа в панель администратора.
    Vlad Simonov, 2016
*/
var currentLang;

var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Идентификатор администратора для будущей реализации нескольких администраторов
    $scope.field = {id: 1};
    
    // Авторизация пользователя
    $scope.login = function () {
        if ($scope.field.password && $scope.field.id) {
            $.post('api?act=login', $scope.field, function (response) {
                var object = JSON.parse(response);
                if (object.status) {
                    document.location.href = 'admin';
                }
            });
        }
    }
    
    // Если администратор авторизован, то перенаправляем в панель администратора
    function checkAuth () {
        var token = $cookies.get('token'),
            id = $cookies.get('id');
        if (token && id) {
            $.post('api?act=check-auth', {token: token, id: id}, function (response) {
                var object = JSON.parse(response);
                if (object.status)
                    document.location.href = 'admin';
            });
        }
    }
    checkAuth();
}]);