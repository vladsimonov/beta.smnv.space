/*
    Функции, использующиеся в каждом разделе сайта.
    Vlad Simonov, 2016
*/

// Пробует получить язык системы, если язык отличен от русского и английского,
// то ставится английский язык
function checkLanguage (cookies, scope, trService) {
    // Когда cookie [lang] отсутствует
    if (!cookies.get('lang')) {
        var systemLanguage = window.navigator.language;
        if (systemLanguage == 'en' || systemLanguage == 'ru')
            setCookie(cookies, 'lang', systemLanguage);
        else
            setCookie(cookies, 'lang', 'en');
    }
    // Получение перевода из JSON
    trService.getTranslation(scope, cookies.get('lang'));
    scope.currentLang = cookies.get('lang');
    // Объекты для отображения противоположного установленному языка
    if (cookies.get('lang') == 'ru')
        scope.reverseLang = ['en', 'English'];
    else
        scope.reverseLang = ['ru', 'Русский'];
}

// Изменение языка
function changeLang(lang, cookies, trService, scope) {
    setCookie(cookies, 'lang', lang);
    trService.getTranslation(scope, lang);
    scope.currentLang = cookies.get('lang');

    if (cookies.get('lang') == 'ru')
        scope.reverseLang = ['en', 'English'];
    else
        scope.reverseLang = ['ru', 'Русский'];
}

// Установка cookie
function setCookie($cookies, key, value) {
    var now = new Date();
    now.setTime(now.getTime() + 3600000 * 24 * 1000);
    $cookies.put(key, value, {expires: now});
}

// Убирает overlay с загрузкой через 500ms
window.addEventListener('load', function () {
    setTimeout(function () {
        $('.preload').fadeOut(300);
        $('body').removeAttr('style');
    }, 500);
});