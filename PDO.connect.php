<?php
$host = 'localhost';
$dbname = 'smnv.space';
$username = 'root';
$password = '';
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
);

$pdo = new PDO("mysql:host=".$host.";dbname=".$dbname, $username, $password, $options);

?>