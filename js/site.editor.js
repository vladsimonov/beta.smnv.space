/*
    Контроллер редактора записей.
    Vlad Simonov, 2016
*/
var app = angular.module('app', ['translation', 'ngCookies', 'ngResource', 'ngAnimate']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', '$animate', function ($scope, $cookies, $http, $resource, translationService, $animate){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Функция, получающая параметры из URL
    function getParams () {
        var regexp = /[?&]([^=#]+)=([^&#]*)/g,
            url = location.href,
            params = {},
            match;
        while(match = regexp.exec(url)) {
            params[match[1]] = match[2];
        }
        return prepareParams(params);
    }
    
    // Парсинг параметров для определения состояния редактора - создание или редактирование
    function prepareParams (params) {
        var preparedParams = {};
        if (params.type == 'project' || params.type == 'blog') {
            if (params.act == 'create' && params.act != undefined)
                preparedParams = {act: 'create', type: params.type};
            else 
                if (params.act == 'edit' && params.id)
                    preparedParams = params;
                else
                    preparedParams = {act: 'create'};
        }
        else preparedParams = {act: 'create', type: 'project'};
        
        analysParams(preparedParams);
        return preparedParams;
    }
    
    $scope.params = getParams();
    
    // Создание пустого объекта редактора или получение объекта записи
    // для редактирования. Если POST запрос вернет {status: 0}, то запись не существует,
    // переводим 
    function analysParams(params) {
        switch (params.act) {
            case 'create':
                $scope.editor = {name: '', type: '', html: 'Start type your post'};
                break;
            case 'edit':
                $.post('api?act=get-post', {type: params.type, id: params.id}, function (response) {
                    var object = JSON.parse(response);
                    // Если запись существует
                    if (object.status) {
                        $scope.editor = object.post;
                        $('.editor').html($scope.editor.full_text);
                    } 
                    // Если запись не найдена
                    else {
                        $scope.params = {act: 'create', type: 'project'};
                        $scope.editor = {name: '', type: '', html: 'Start type your post'};
                    }
                });
                break;
        }
    }
    
    // Массив изображений
    $scope.imageList = [];
    
    // Функция проверки данных администратора
    function checkAuth () {
        var token = $cookies.get('token'),
            id = $cookies.get('id');
        if (token && id) {
            $.post('api?act=check-auth', {token: token, id: id}, function (response) {
                var object = JSON.parse(response);
                if (!object.status)
                    document.location.href = 'panel';
            });
        } 
        else
            document.location.href = 'panel';
    }
    checkAuth();
    
    // Метод сохранения записи в базу данных
    $scope.submit = function () {
        $scope.editor.full_text = $('.editor').html();
        $scope.editor.post_type = $scope.params.type;
        $scope.editor.date = new Date().getTime();
        
        if ($scope.editor.full_text) {
            switch ($scope.params.act) {
                case 'create':
                    $.post('api?act=create-post', $scope.editor, function (response) {
                        var object = JSON.parse(response);
                        if (object.status) document.location.href = $scope.params.type + '/' + $scope.editor.url;
                    });
                    break;
                case 'edit':
                    $.post('api?act=update-post', $scope.editor, function (response) {
                        var object = JSON.parse(response);
                        if (object.status) document.location.href = $scope.params.type + '/' + $scope.editor.url;
                    });
                    break;
                default:
                    return 0;
                    break;
            }
        }
    }
    
    // Создание редактора
    var editor = new MediumEditor('.editor', {
        toolbar: {
            buttons: ['bold', 'italic', 'unorderedlist', 'h1', 'h2', 'h3', 'anchor', 'image', 
            {
            name: 'removeFormat',
            aria: 'remove formatting',
            action: 'removeFormat',
            contentDefault: '<b>X</b>',
            contentFA: '<i class="fa fa-eraser"></i>'
            }, 'bq']
        },
        buttonLabels: 'fontawesome',
        extensions: {
            bq: new BlockquoteEditor()
        }
    });
    
    // Обработчик события, когда выбран файл в input[type=file]
    $('#userfile').on('change', function (e) {
        var file_data = $('#userfile').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);                        
        $.ajax({
            url: 'upload.php', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'POST',
            success: function(response){
                var object = JSON.parse(response);
                $scope.imageList.push({src: object.src, time: new Date().getTime(), style: {'background-image': 'url("' + object.src + '")'}});
                $scope.$apply();
            }
         });
    });
}]);