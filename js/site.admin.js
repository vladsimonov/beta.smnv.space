/*
    Контроллер панели администратора.
    Vlad Simonov, 2016
*/
var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Метод для удаления поста
    $scope.removePost = function (id) {
        alert('sadf');
        $.post('api?act=remove-post', {type: $scope.section, id: id}, function (response) {
            var object = JSON.parse(response);
            if (object.status)
                window.location.reload();
        });
    }
    
    // Функция проверки данных администратора
    function checkAuth () {
        var token = $cookies.get('token'),
            id = $cookies.get('id');
        if (token && id) {
            $.post('api?act=check-auth', {token: token, id: id}, function (response) {
                var object = JSON.parse(response);
                if (!object.status)
                    document.location.href = 'panel';
            });
        } 
        else
            document.location.href = 'panel';
    }
    checkAuth();
    
    // Получение записей проектов и блога
    $scope.admin = {};
    function getAdmin () {
        // Получение записей проектов
        $.post('api?act=get-posts', function (response) {
            var object = JSON.parse(response);
            if (object.status) {
                $scope.admin.projects = object.project;
            }
        });
        // Получение записей блога
        $.post('api?act=get-posts&t=blog', function (response) {
            var object = JSON.parse(response);
            if (object.status) {
                $scope.admin.blogs = object.blog;
            }
        });
    }
    getAdmin();
}]);