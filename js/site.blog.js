/*
    Контроллер страницы блога.
    Vlad Simonov, 2016
*/
var app = angular.module('app', ['translation', 'ngCookies', 'ngResource']);

app.controller('appCtrl', ['$scope', '$cookies', '$http', '$resource', 'translationService', function ($scope, $cookies, $http, $resource, translationService){
    // Получает объект lang из JSON файла языка (из /lang)
    checkLanguage($cookies, $scope, translationService);
    
    // Метод изменения языка сайта
    $scope.changeLang = function (lang) {
        changeLang(lang, $cookies, translationService, $scope);
    }
    
    // Получает все записи из блога для отображения превью
    function getPosts () {
        // По умолчанию POST запрос по URL 'api?act=get-posts' вернет записи проектов,
        // поэтому, нужно указать тип записей t=blog
        $.post('api?act=get-posts&t=blog', function (response) {
            var object = JSON.parse(response);
            if (object.status) {
                $scope.blogs = object.blog;
                $scope.$apply();
            }
        });
    }
    getPosts();
}]);